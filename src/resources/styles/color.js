/**
 * color.js - Shared Application colors
 */
export const color = {
  black: '#000000',
  white: '#FFFFFF',
  primary: '#AAAAAA',
  blue: '#A6EEF5',
  purple: 'rgb(120,36,221)',
  pink: 'rgb(253,85,178)',
  text: 'rgb(46,59,65)',
  gray: '#F7F9F9',
  lightGray: '#B3B6B7',
  navGrey: 'rgb(84,95,99)',
  loginLabel: 'rgb(216,216,216)',
  transparent: 'transparent',
  backgroundGrey: '#F2F6FA',
  backgroundGreyRGB: 'rgb(241,246,250)',
  greyBlue: '#34364C',
  divider: 'rgb(151,151,151)',
  blueRGB: 'rgb(0,122,255)',
  shareIconAndroid: '#545F63',
  whiteRGBA: 'rgba(255,255,255,0.98)',
  robinEggBlue: 'rgb(135,230,240)',
  textBlack: 'rgb(60,60,69)'
}
