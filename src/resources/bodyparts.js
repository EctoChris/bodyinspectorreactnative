export default {
  BODYPARTS: {
      FEMALE: {
          FRONT: {
            UPPERARM:{
              RIGHT: 'upperarmright',
              LEFT: 'upperarmleft'
            },
            FOREARM: {
              RIGHT: 'forearmright',
              LEFT: 'forearmleft'
            },
            HAND: {
              RIGHT: 'handright',
              LEFT: 'handleft'
            },
            THIGH: {
              RIGHT: 'thighright',
              LEFT: 'thighleft'
            },
            KNEE: {
              RIGHT: 'kneeright',
              LEFT: 'kneeleft'
            },
            FOOT: {
              RIGHT: 'footright',
              LEFT: 'footleft'
            },
            SHIN: {
              RIGHT: 'shinright',
              LEFT: 'shinleft'
            },
            ABS: 'abdominals',
            GROIN:'groin',
            GENITALS: 'genitals',
            CHEST:'chest',
            UPPERSTOMACHONE: 'upperstomach',
            UPPERSTOMACHTWO: 'upperstomach_',
            BREAST: {
              RIGHT: 'breastright',
              LEFT: 'breastleft'
            },
            NECK: 'neck',
            FACE: 'face',
            NONE: 'none'
          },
          BACK: {
            ANUS: 'anus',
            BUTTOCKS:' buttocks',
            CALF: {
              RIGHT: 'calfright',
              LEFT: 'calfleft'
            },
            ELBOW: {
              RIGHT: 'elbowright',
              LEFT: 'elbowleft'
            },
            FOOT: {
              RIGHT: 'footright',
              LEFT: 'footleft'
            },
            HAND: {
              RIGHT: 'handright',
              LEFT: 'handleft'
            },
            LOWERARM: {
              RIGHT: 'lowerarmright',
              LEFT: 'lowerarmleft'
            },
            LOWERBACK: 'lowerback',
            NECK: 'neck',
            THIGH: {
              RIGHT: 'thighright',
              LEFT: 'thighleft'
            },
            UPPERARM: {
              RIGHT: 'upperarmright',
              LEFT: 'upperarmleft'
            },
            UPPERBACK: 'upperback',
            NONE: 'none'
          }     
        }   
      }
  }
  