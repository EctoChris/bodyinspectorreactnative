import React, {Component} from 'react'
import {StyleSheet, Image, View, TouchableOpacity} from 'react-native' //Add TouchableOpacity for testing
// import {inject, observer} from 'mobx-react'
// import { Global } from '@jest/types';
// import TouchableAccessibility from '../Accessibility/TouchableAccessibility'

//Image Paths:
import { color } from '../resources/styles/color'
import defaultBody from '../assets/images/femaleStomach.png'
import abs from '../assets/images/femaleStomach.png'
import upperarm from '../assets/images/femaleUpperArms.png'
import forearm from '../assets/images/femaleLowerArms.png'
import genitalsImage from '../assets/images/femaleGenitals.png'
import breastsImage from '../assets/images/femaleBreasts.png'
import hands from '../assets/images/femaleHands.png'
import chestImage from '../assets/images/femaleChest.png'
import thighsImage from '../assets/images/femaleThighs.png'
import kneesImage from '../assets/images/femaleKnees.png'
import shinsImage from '../assets/images/femaleShins.png'
import feetImage from '../assets/images/femaleFeet.png'
import groinImage from '../assets/images/femaleGroin.png'
import upperStomachImage from '../assets/images/femaleRibs.png'
import neckImage from'../assets/images/femaleNeck.png'
import faceImage from '../assets/images/femaleFace.png'
import GLOBALS from '../resources/bodyparts'

type Props = {}

export default
// @inject('store')
// @observer
class BodyInspectorFemaleFront extends Component<Props> {
  constructor(props) {
    super(props)
    this.originalRatio = 2.1214
    let bodyPartTemplate = { marginLeft: 0, marginTop: 0, width: 0, height: 0 }
    let bodyParts = [
      GLOBALS.BODYPARTS.FEMALE.FRONT.UPPERARM.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.UPPERARM.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FOREARM.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FOREARM.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.HAND.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.HAND.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.THIGH.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.THIGH.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.KNEE.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.KNEE.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FOOT.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FOOT.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.SHIN.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.SHIN.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.GROIN,
      GLOBALS.BODYPARTS.FEMALE.FRONT.GENITALS,
      GLOBALS.BODYPARTS.FEMALE.FRONT.ABS,
      GLOBALS.BODYPARTS.FEMALE.FRONT.CHEST,
      GLOBALS.BODYPARTS.FEMALE.FRONT.BREAST.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.BREAST.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.NECK,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FACE,
      GLOBALS.BODYPARTS.FEMALE.FRONT.NONE
    ]
    let bodyPartsNew = new Object()
    for (let i = 0; i < bodyParts.length; i++)
    {
      bodyPartsNew[bodyParts[i]] = bodyPartTemplate
    }
    this.state = {
      bodyParts: bodyPartsNew,
      currentImage: defaultBody,
      currentSelectedBodyPart: GLOBALS.BODYPARTS.NONE
    }
  }

  find_dimensions(layout) {
    //Find view dimensions {width, height} at renderTime
    console.log('THE LAYOUT IS', layout)
    //Calculating the space around an image for responsiveness
    let viewRatio = layout.height / layout.width
    let spaceAtTop = 0
    let spaceAtLeft = 0
    let currentWidthOfImage = 0
    let currentHeightOfImage = 0
    if (viewRatio >= this.originalRatio)
    {
      currentHeightOfImage = this.originalRatio * layout.width
      currentWidthOfImage = layout.width
      let diffViewHeightAndImage = layout.height - currentHeightOfImage
      spaceAtTop = diffViewHeightAndImage / 2
      //yScale = (layout.height - diffViewHeightAndImage) / originalImageHeight
    }
    else {
      currentWidthOfImage = layout.height / this.originalRatio
      currentHeightOfImage = layout.height
      let diffViewWidthAndImage = layout.width - currentWidthOfImage
      spaceAtLeft = diffViewWidthAndImage / 2
    }
    console.log('====Current Image Dimensions =====')
    console.log('Height = ' + currentHeightOfImage)
    console.log('Width = ' + currentWidthOfImage)
    //Assign all body parts dimensions and positioning on image:
    let rightHand =     { marginLeft: spaceAtLeft + (0.001 * currentWidthOfImage), marginTop: spaceAtTop + (0.462 * currentHeightOfImage), width: 0.1985 * currentWidthOfImage, height: 0.0943 * currentHeightOfImage, image: hands, key: GLOBALS.BODYPARTS.FEMALE.FRONT.HAND.RIGHT}
    let leftHand = { marginLeft: spaceAtLeft + (0.8 * currentWidthOfImage), marginTop: spaceAtTop + (0.462 * currentHeightOfImage), width: 0.1985 * currentWidthOfImage, height: 0.0943 * currentHeightOfImage, image: hands, key: GLOBALS.BODYPARTS.FEMALE.FRONT.HAND.LEFT}
    let rightForearm =  { marginLeft: spaceAtLeft + (0.0803 * currentWidthOfImage), marginTop: spaceAtTop + (0.3787 * currentHeightOfImage), width: 0.1985 * currentWidthOfImage, height: 0.08417 * currentHeightOfImage, image: forearm, key: GLOBALS.BODYPARTS.FEMALE.FRONT.FOREARM.RIGHT}
    let rightUpperarm = { marginLeft: spaceAtLeft + (0.155 * currentWidthOfImage), marginTop: spaceAtTop + (0.174 * currentHeightOfImage),   width: 0.1985 * currentWidthOfImage, height: 0.203 * currentHeightOfImage, image: upperarm, key: GLOBALS.BODYPARTS.FEMALE.FRONT.UPPERARM.RIGHT}
    let leftForearm = { marginLeft: spaceAtLeft + (0.745 * currentWidthOfImage), marginTop: spaceAtTop + (0.3787 * currentHeightOfImage),   width: 0.1985 * currentWidthOfImage, height: 0.08417 * currentHeightOfImage, image: forearm, key: GLOBALS.BODYPARTS.FEMALE.FRONT.FOREARM.LEFT}
    let leftUpperarm = { marginLeft: spaceAtLeft + (0.645 * currentWidthOfImage), marginTop: spaceAtTop + (0.174 * currentHeightOfImage),   width: 0.1985 * currentWidthOfImage, height: 0.203 * currentHeightOfImage, image: upperarm, key: GLOBALS.BODYPARTS.FEMALE.FRONT.UPPERARM.LEFT}
    let abdominals    = { marginLeft: spaceAtLeft + (0.3167 * currentWidthOfImage), marginTop: spaceAtTop + (0.33838 * currentHeightOfImage), width: 0.375 * currentWidthOfImage,  height: 0.0865 * currentHeightOfImage, image: abs, key: GLOBALS.BODYPARTS.FEMALE.FRONT.ABS}
    let groin = { marginLeft: spaceAtLeft + (0.3167 * currentWidthOfImage), marginTop: spaceAtTop + (0.42 * currentHeightOfImage), width: 0.375 * currentWidthOfImage,  height: 0.07 * currentHeightOfImage, image: groinImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.GROIN}
    let genitals = { marginLeft: spaceAtLeft + (0.45 * currentWidthOfImage), marginTop: spaceAtTop + (0.48 * currentHeightOfImage), width: 0.1 * currentWidthOfImage,  height: 0.08 * currentHeightOfImage, image: genitalsImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.GENITALS}
    let upperstomachOne = { marginLeft: spaceAtLeft + (0.3167 * currentWidthOfImage), marginTop: spaceAtTop + (0.3 * currentHeightOfImage), width: 0.375 * currentWidthOfImage,  height: 0.04225 * currentHeightOfImage, image: upperStomachImage, key: GLOBALS.BODYPARTS.UPPERSTOMACHONE}
    let upperstomachTwo = { marginLeft: spaceAtLeft + (0.47 * currentWidthOfImage), marginTop: spaceAtTop + (0.255 * currentHeightOfImage), width: 0.06 * currentWidthOfImage,  height: 0.0865 * currentHeightOfImage, image: upperStomachImage, key: GLOBALS.BODYPARTS.UPPERSTOMACHTWO}
    let leftBreast = { marginLeft: spaceAtLeft + (0.53 * currentWidthOfImage), marginTop: spaceAtTop + (0.230 * currentHeightOfImage), width: 0.125 * currentWidthOfImage,  height: 0.07 * currentHeightOfImage, image: breastsImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.BREAST.LEFT}
    let rightBreast = { marginLeft: spaceAtLeft + (0.35 * currentWidthOfImage), marginTop: spaceAtTop + (0.230 * currentHeightOfImage), width: 0.125 * currentWidthOfImage,  height: 0.07 * currentHeightOfImage, image: breastsImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.BREAST.RIGHT}
    let chest = { marginLeft: spaceAtLeft + (0.3167 * currentWidthOfImage), marginTop: spaceAtTop + (0.17 * currentHeightOfImage), width: 0.375 * currentWidthOfImage,  height: 0.083 * currentHeightOfImage, image: chestImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.CHEST}
    let rightThigh = { marginLeft: spaceAtLeft + (0.3 * currentWidthOfImage), marginTop: spaceAtTop + (0.48 * currentHeightOfImage), width: 0.15 * currentWidthOfImage,  height: 0.2 * currentHeightOfImage, image: thighsImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.THIGH.RIGHT}
    let leftThigh = { marginLeft: spaceAtLeft + (0.55 * currentWidthOfImage), marginTop: spaceAtTop + (0.48 * currentHeightOfImage), width: 0.15 * currentWidthOfImage,  height: 0.2 * currentHeightOfImage, image: thighsImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.THIGH.LEFT}
    let rightKnee = { marginLeft: spaceAtLeft + (0.3 * currentWidthOfImage), marginTop: spaceAtTop + (0.68 * currentHeightOfImage), width: 0.15 * currentWidthOfImage,  height: 0.06 * currentHeightOfImage, image: kneesImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.KNEE.RIGHT}
    let leftKnee = { marginLeft: spaceAtLeft + (0.555 * currentWidthOfImage), marginTop: spaceAtTop + (0.68 * currentHeightOfImage), width: 0.15 * currentWidthOfImage,  height: 0.06 * currentHeightOfImage, image: kneesImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.KNEE.LEFT}
    let rightShin = { marginLeft: spaceAtLeft + (0.275 * currentWidthOfImage), marginTop: spaceAtTop + (0.72 * currentHeightOfImage), width: 0.18 * currentWidthOfImage,  height: 0.165 * currentHeightOfImage, image: shinsImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.SHIN.RIGHT}
    let leftShin = { marginLeft: spaceAtLeft + (0.58 * currentWidthOfImage), marginTop: spaceAtTop + (0.72 * currentHeightOfImage), width: 0.18 * currentWidthOfImage,  height: 0.165 * currentHeightOfImage, image: shinsImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.SHIN.LEFT}
    let rightFoot = { marginLeft: spaceAtLeft + (0.25 * currentWidthOfImage), marginTop: spaceAtTop + (0.87 * currentHeightOfImage), width: 0.15 * currentWidthOfImage,  height: 0.13 * currentHeightOfImage, image: feetImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.FOOT.RIGHT}
    let leftFoot = { marginLeft: spaceAtLeft + (0.65 * currentWidthOfImage), marginTop: spaceAtTop + (0.87 * currentHeightOfImage), width: 0.15 * currentWidthOfImage,  height: 0.13 * currentHeightOfImage, image: feetImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.FOOT.LEFT}
    let neck =  { marginLeft: spaceAtLeft + (0.3949 * currentWidthOfImage), marginTop: spaceAtTop + (0.13 * currentHeightOfImage), width: 0.207 * currentWidthOfImage, height: 0.06 * currentHeightOfImage, image: neckImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.NECK}
    let face = { marginLeft: spaceAtLeft + (0.36 * currentWidthOfImage), marginTop: spaceAtTop + (0.02 * currentHeightOfImage), width: 0.28 * currentWidthOfImage, height: 0.120 * currentHeightOfImage, image:faceImage, key: GLOBALS.BODYPARTS.FEMALE.FRONT.FACE}

    let bodyParts = [
      rightUpperarm,
      leftUpperarm,
      rightForearm,
      leftForearm,
      rightHand,
      leftHand,
      abdominals,
      groin,
      genitals,
      upperstomachOne,
      upperstomachTwo,
      chest,
      rightThigh,
      leftThigh,
      rightKnee,
      leftKnee,
      rightShin,
      leftShin,
      rightFoot,
      leftFoot,
      neck,      
      leftBreast,
      rightBreast,
      face
    ]

    let bodyPartKeys = [
      GLOBALS.BODYPARTS.FEMALE.FRONT.UPPERARM.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.UPPERARM.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FOREARM.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FOREARM.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.HAND.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.HAND.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.ABS,
      GLOBALS.BODYPARTS.FEMALE.FRONT.GROIN,
      GLOBALS.BODYPARTS.FEMALE.FRONT.GENITALS,
      GLOBALS.BODYPARTS.FEMALE.FRONT.UPPERSTOMACHONE,
      GLOBALS.BODYPARTS.FEMALE.FRONT.UPPERSTOMACHTWO,
      GLOBALS.BODYPARTS.FEMALE.FRONT.CHEST,
      GLOBALS.BODYPARTS.FEMALE.FRONT.THIGH.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.THIGH.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.KNEE.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.KNEE.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.SHIN.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.SHIN.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FOOT.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FOOT.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.NECK,
      GLOBALS.BODYPARTS.FEMALE.FRONT.BREAST.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.BREAST.LEFT,
      GLOBALS.BODYPARTS.FEMALE.FRONT.FACE,
      GLOBALS.BODYPARTS.FEMALE.FRONT.NONE
    ]

    let bodyPartsNew = new Object()
    for (let i = 0; i < bodyParts.length; i++)
    {
      bodyPartsNew[bodyPartKeys[i]] = bodyParts[i]
    }
    console.log('======final body parts=======')
    console.log(bodyPartsNew)
    this.setState({bodyParts: bodyPartsNew})
  }

  showAbs(){
    console.warn('clicked red box')
    console.log('clicked the red box')
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }
  
  changeImage(image, key){
    console.log(key)
    this.setState({currentImage: image})
    this.setState({currentSelectedBodyPart: key})
  }

  render(){
    const { buttons, imageStyle } = styles

    return (
      <View style={styles.container}>

        <Image
          onLayout={(event) => {this.find_dimensions(event.nativeEvent.layout)}}
          source={this.state.currentImage}
          resizeMode="contain"
          style={imageStyle}>
        </Image>
        {
          Object.keys(this.state.bodyParts).map((bodyPart) => {
            const key = bodyPart
            return (
              <TouchableOpacity
                key={key}
                style={[buttons, {marginLeft: this.state.bodyParts[key].marginLeft, top: this.state.bodyParts[key].marginTop, width: this.state.bodyParts[key].width, height: this.state.bodyParts[key].height}]}
                onPress={() => this.changeImage(this.state.bodyParts[key].image, this.state.bodyParts[key].key)}
              />
            )
          })
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
  },
    buttons: {
      position: 'absolute',
      zIndex: 4,
      // backgroundColor: 'rgba(52, 52, 52, 0.8)',
    },
    imageStyle: {
      flex: 1,
      height: undefined,
      width: undefined,
      padding: 0,
      margin: 0,
      borderWidth: 1,
      borderColor: color.black
    },

})

{/*<TouchableOpacity onPress={() => this.showAbs()}*/}
{/*style={buttons}>*/}
{/*<Text style={buttonInner}> Hello ! </Text>*/}
{/*</TouchableOpacity>*/}
