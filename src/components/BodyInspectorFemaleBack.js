import React, {Component} from 'react'
import {StyleSheet, Image, View, TouchableOpacity} from 'react-native' //Add TouchableOpacity for testing
// import {inject, observer} from 'mobx-react'
// import { Global } from '@jest/types';
// import TouchableAccessibility from '../Accessibility/TouchableAccessibility'

//Image Paths:
import { color } from '../resources/styles/color'
import defaultBody from '../assets/images/femaleBack.png'
import anusImage from '../assets/images/femaleBackAnus.png'
import buttocksImage from '../assets/images/femaleBackButtocks.png'
import calvesImage from '../assets/images/femaleBackCalves.png'
import elbowsImage from '../assets/images/femaleBackElbows.png'
import feetImage from '../assets/images/femaleBackFeet.png'
import handsImage from '../assets/images/femaleBackHands.png'
import lowerBackImage from '../assets/images/femaleBackLower.png'
import lowerArmsImage from '../assets/images/femaleBackLowerArms.png'
import neckImage from '../assets/images/femaleBackNeck.png'
import thighsImage from '../assets/images/femaleBackThighs.png'
import upperBackImage from '../assets/images/femaleBackUpper.png'
import upperArmsImage from '../assets/images/femaleBackUpperArms.png'

import GLOBALS from '../resources/bodyparts'

type Props = {}


export default
// @inject('store')
// @observer
class BodyInspectorFemaleFront extends Component<Props> {
  constructor(props) {
    super(props)
    this.originalRatio = 2.1214
    let bodyPartTemplate = { marginLeft: 0, marginTop: 0, width: 0, height: 0 }
    let bodyParts = [
      GLOBALS.BODYPARTS.FEMALE.BACK.ANUS,
      GLOBALS.BODYPARTS.FEMALE.BACK.BUTTOCKS,
      GLOBALS.BODYPARTS.FEMALE.BACK.CALF.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.BACK.CALF.LEFT,
      GLOBALS.BODYPARTS.FEMALE.BACK.ELBOW.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.BACK.ELBOW.LEFT,
      GLOBALS.BODYPARTS.FEMALE.BACK.FOOT.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.BACK.FOOT.LEFT,
      GLOBALS.BODYPARTS.FEMALE.BACK.HAND.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.BACK.HAND.LEFT,
      GLOBALS.BODYPARTS.FEMALE.BACK.LOWERARM.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.BACK.LOWERARM.LEFT,
      GLOBALS.BODYPARTS.FEMALE.BACK.LOWERBACK,
      GLOBALS.BODYPARTS.FEMALE.BACK.NECK,
      GLOBALS.BODYPARTS.FEMALE.BACK.THIGH.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.BACK.THIGH.LEFT,
      GLOBALS.BODYPARTS.FEMALE.BACK.UPPERARM.RIGHT,
      GLOBALS.BODYPARTS.FEMALE.BACK.UPPERARM.LEFT,
      GLOBALS.BODYPARTS.FEMALE.BACK.UPPERBACK,
      GLOBALS.BODYPARTS.FEMALE.BACK.NONE,
    ]
    let bodyPartsNew = new Object()
    for (let i = 0; i < bodyParts.length; i++)
    {
      bodyPartsNew[bodyParts[i]] = bodyPartTemplate
    }
    this.state = {
      bodyParts: bodyPartsNew,
      currentImage: defaultBody,
      currentSelectedBodyPart: GLOBALS.BODYPARTS.NONE
    }
  }

  find_dimensions(layout) {
    //Find view dimensions {width, height} at renderTime
    console.log('THE LAYOUT IS', layout)
    //Calculating the space around an image for responsiveness
    let viewRatio = layout.height / layout.width
    let spaceAtTop = 0
    let spaceAtLeft = 0
    let currentWidthOfImage = 0
    let currentHeightOfImage = 0
    if (viewRatio >= this.originalRatio)
    {
      currentHeightOfImage = this.originalRatio * layout.width
      currentWidthOfImage = layout.width
      let diffViewHeightAndImage = layout.height - currentHeightOfImage
      spaceAtTop = diffViewHeightAndImage / 2
    }
    else {
      currentWidthOfImage = layout.height / this.originalRatio
      currentHeightOfImage = layout.height
      let diffViewWidthAndImage = layout.width - currentWidthOfImage
      spaceAtLeft = diffViewWidthAndImage / 2
    }
    console.log('====Current Image Dimensions =====')
    console.log('Height = ' + currentHeightOfImage)
    console.log('Width = ' + currentWidthOfImage)
    //Assign all body parts dimensions and positioning on image:
    // let rightHand =     { marginLeft: spaceAtLeft + (0.001 * currentWidthOfImage), marginTop: spaceAtTop + (0.462 * currentHeightOfImage), width: 0.1985 * currentWidthOfImage, height: 0.0943 * currentHeightOfImage, image: hands, key: GLOBALS.BODYPARTS.FEMALE.FRONT.HAND.RIGHT}
    let handright = { marginLeft: spaceAtLeft + (0.76114 * currentWidthOfImage), marginTop: spaceAtTop + (0.475 * currentHeightOfImage), width: 0.207 * currentWidthOfImage, height: 0.1 * currentHeightOfImage, image: handsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.HAND.RIGHT}
    let handleft = { marginLeft: spaceAtLeft + (0.0286 * currentWidthOfImage), marginTop: spaceAtTop + (0.475 * currentHeightOfImage), width: 0.207 * currentWidthOfImage, height: 0.1 * currentHeightOfImage, image: handsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.HAND.LEFT}
    let lowerarmleft = { marginLeft: spaceAtLeft + (0.091 * currentWidthOfImage), marginTop: spaceAtTop + (0.387 * currentHeightOfImage), width: 0.1984 * currentWidthOfImage, height: 0.096 * currentHeightOfImage, image: lowerArmsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.LOWERARM.RIGHT}
    let lowerarmright = { marginLeft: spaceAtLeft + (0.6974 * currentWidthOfImage), marginTop: spaceAtTop + (0.387 * currentHeightOfImage), width: 0.1984 * currentWidthOfImage, height: 0.096 * currentHeightOfImage, image: lowerArmsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.LOWERARM.LEFT}
    let elbowleft = { marginLeft: spaceAtLeft + (0.1442 * currentWidthOfImage), marginTop: spaceAtTop + (0.3376 * currentHeightOfImage), width: 0.186 * currentWidthOfImage, height: 0.053 * currentHeightOfImage, image: elbowsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.ELBOW.LEFT}
    let elbowright = { marginLeft: spaceAtLeft + (0.66 * currentWidthOfImage), marginTop: spaceAtTop + (0.3376 * currentHeightOfImage), width: 0.186 * currentWidthOfImage, height: 0.053 * currentHeightOfImage, image: elbowsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.ELBOW.RIGHT}
    let upperarmleft = { marginLeft: spaceAtLeft + (0.1729 * currentWidthOfImage), marginTop: spaceAtTop + (0.186 * currentHeightOfImage), width: 0.1697 * currentWidthOfImage, height: 0.153 * currentHeightOfImage, image: upperArmsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.UPPERARM.LEFT}
    let upperarmright = { marginLeft: spaceAtLeft + (0.642 * currentWidthOfImage), marginTop: spaceAtTop + (0.186 * currentHeightOfImage), width: 0.1697 * currentWidthOfImage, height: 0.153 * currentHeightOfImage, image: upperArmsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.UPPERARM.RIGHT}
    let upperback = { marginLeft: spaceAtLeft + (0.33 * currentWidthOfImage), marginTop: spaceAtTop + (0.1816 * currentHeightOfImage), width: 0.3121 * currentWidthOfImage, height: 0.1805 * currentHeightOfImage, image: upperBackImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.UPPERBACK}
    let lowerback = { marginLeft: spaceAtLeft + (0.3175 * currentWidthOfImage), marginTop: spaceAtTop + (0.353 * currentHeightOfImage), width: 0.350 * currentWidthOfImage, height: 0.0916 * currentHeightOfImage, image: lowerBackImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.LOWERBACK}
    let buttocks = { marginLeft: spaceAtLeft + (0.2929 * currentWidthOfImage), marginTop: spaceAtTop + (0.445 * currentHeightOfImage), width: 0.3949 * currentWidthOfImage, height: 0.116 * currentHeightOfImage, image: buttocksImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.BUTTOCKS}
    let thighleft = { marginLeft: spaceAtLeft + (0.2758 * currentWidthOfImage), marginTop: spaceAtTop + (0.5583 * currentHeightOfImage), width: 0.191 * currentWidthOfImage, height: 0.193 * currentHeightOfImage, image: thighsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.THIGH.LEFT}
    let thighright = { marginLeft: spaceAtLeft + (0.5242 * currentWidthOfImage), marginTop: spaceAtTop + (0.5583 * currentHeightOfImage), width: 0.191 * currentWidthOfImage, height: 0.193 * currentHeightOfImage, image: thighsImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.THIGH.RIGHT}
    let calfleft = { marginLeft: spaceAtLeft + (0.2707 * currentWidthOfImage), marginTop: spaceAtTop + (0.75 * currentHeightOfImage), width: 0.191 * currentWidthOfImage, height: 0.1925 * currentHeightOfImage, image: calvesImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.CALF.LEFT}
    let calfright = { marginLeft: spaceAtLeft + (0.5254 * currentWidthOfImage), marginTop: spaceAtTop + (0.75 * currentHeightOfImage), width: 0.191 * currentWidthOfImage, height: 0.1925 * currentHeightOfImage, image: calvesImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.CALF.RIGHT}
    let footleft = { marginLeft: spaceAtLeft + (0.2579 * currentWidthOfImage), marginTop: spaceAtTop + (0.933 * currentHeightOfImage), width: 0.2019 * currentWidthOfImage, height: 0.068 * currentHeightOfImage, image: feetImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.FOOT.LEFT}
    let footright = { marginLeft: spaceAtLeft + (0.5254 * currentWidthOfImage), marginTop: spaceAtTop + (0.933 * currentHeightOfImage), width: 0.2019 * currentWidthOfImage, height: 0.068 * currentHeightOfImage, image: feetImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.FOOT.RIGHT}
    let anus = { marginLeft: spaceAtLeft + (0.4369 * currentWidthOfImage), marginTop: spaceAtTop + (0.5116 * currentHeightOfImage), width: 0.1114 * currentWidthOfImage, height: 0.0688 * currentHeightOfImage, image: anusImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.ANUS}
    let neck = { marginLeft: spaceAtLeft + (0.3535 * currentWidthOfImage), marginTop: spaceAtTop + (0.1175 * currentHeightOfImage), width: 0.257 * currentWidthOfImage, height: 0.0666 * currentHeightOfImage, image: neckImage, key: GLOBALS.BODYPARTS.FEMALE.BACK.NECK}

    let bodyParts = [
      buttocks,
      calfright,
      calfleft,
      elbowright,
      elbowleft,
      footright,
      footleft,
      handright,
      handleft,
      lowerarmright,
      lowerarmleft,
      lowerback,
      neck,
      thighright,
      thighleft,
      upperarmright,
      upperarmleft,
      upperback,      
      anus
    ]

    let bodyPartKeys = [
        GLOBALS.BODYPARTS.FEMALE.BACK.ANUS,
        GLOBALS.BODYPARTS.FEMALE.BACK.BUTTOCKS,
        GLOBALS.BODYPARTS.FEMALE.BACK.CALF.RIGHT,
        GLOBALS.BODYPARTS.FEMALE.BACK.CALF.LEFT,
        GLOBALS.BODYPARTS.FEMALE.BACK.ELBOW.RIGHT,
        GLOBALS.BODYPARTS.FEMALE.BACK.ELBOW.LEFT,
        GLOBALS.BODYPARTS.FEMALE.BACK.FOOT.RIGHT,
        GLOBALS.BODYPARTS.FEMALE.BACK.FOOT.LEFT,
        GLOBALS.BODYPARTS.FEMALE.BACK.HAND.RIGHT,
        GLOBALS.BODYPARTS.FEMALE.BACK.HAND.LEFT,
        GLOBALS.BODYPARTS.FEMALE.BACK.LOWERARM.RIGHT,
        GLOBALS.BODYPARTS.FEMALE.BACK.LOWERARM.LEFT,
        GLOBALS.BODYPARTS.FEMALE.BACK.LOWERBACK,
        GLOBALS.BODYPARTS.FEMALE.BACK.NECK,
        GLOBALS.BODYPARTS.FEMALE.BACK.THIGH.RIGHT,
        GLOBALS.BODYPARTS.FEMALE.BACK.THIGH.LEFT,
        GLOBALS.BODYPARTS.FEMALE.BACK.UPPERARM.RIGHT,
        GLOBALS.BODYPARTS.FEMALE.BACK.UPPERARM.LEFT,
        GLOBALS.BODYPARTS.FEMALE.BACK.UPPERBACK,
        GLOBALS.BODYPARTS.FEMALE.BACK.NONE,
    ]

    let bodyPartsNew = new Object()
    for (let i = 0; i < bodyParts.length; i++)
    {
      bodyPartsNew[bodyPartKeys[i]] = bodyParts[i]
    }
    console.log('======final body parts=======')
    console.log(bodyPartsNew)
    this.setState({bodyParts: bodyPartsNew})
  }

  showAbs(){
    console.warn('clicked red box')
    console.log('clicked the red box')
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }
  
  changeImage(image, key){
    console.log(key)
    this.setState({currentImage: image})
    this.setState({currentSelectedBodyPart: key})
  }

  render(){
    const { buttons, imageStyle } = styles

    return (
      <View style={styles.container}>

        <Image
          onLayout={(event) => {this.find_dimensions(event.nativeEvent.layout)}}
          source={this.state.currentImage}
          resizeMode="contain"
          style={imageStyle}>
        </Image>
        {
          Object.keys(this.state.bodyParts).map((bodyPart) => {
            const key = bodyPart
            return (
              <TouchableOpacity
                key={key}
                style={[buttons, {marginLeft: this.state.bodyParts[key].marginLeft, top: this.state.bodyParts[key].marginTop, width: this.state.bodyParts[key].width, height: this.state.bodyParts[key].height}]}
                onPress={() => this.changeImage(this.state.bodyParts[key].image, this.state.bodyParts[key].key)}
              />
            )
          })
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
  },
    buttons: {
      position: 'absolute',
      zIndex: 4,
    //   backgroundColor: 'rgba(52, 52, 52, 0.8)',
    },
    imageStyle: {
      flex: 1,
      height: undefined,
      width: undefined,
      padding: 0,
      margin: 0,
      borderWidth: 1,
      borderColor: color.black
    },

})

{/*<TouchableOpacity onPress={() => this.showAbs()}*/}
{/*style={buttons}>*/}
{/*<Text style={buttonInner}> Hello ! </Text>*/}
{/*</TouchableOpacity>*/}
